/*Part 1*/
let firstName = 'John';
console.log("First Name: " + firstName);

let lastName = 'Smith';
console.log("Last Name: " +lastName);

let age1 = 30;
console.log("Age: " +age1);

console.log("Hobbies:");
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobbies);

console.log("Work Address:");
let workAddress = {
	houseNumber: '32',
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}
console.log(workAddress)

/*PART 2*/

	let fullName1 = "Steve Rogers";
	console.log("My full name is: " + fullName1);

	let Age = 40;
	console.log("My current age is: " + Age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		userName: 'captain_america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false

	}
	console.log("My Full Profile: ");
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	let lastLocation = "Atlantic Ocean";
	lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);